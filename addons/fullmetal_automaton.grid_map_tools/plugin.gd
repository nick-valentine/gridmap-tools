tool
extends EditorPlugin

const RAY_LENGTH = 500
const DEFAULT_ACTION_COOLDOWN = 0.2

const Toolbar = preload("res://addons/fullmetal_automaton.grid_map_tools/ui/tools.tscn")
const Utils = preload("res://addons/fullmetal_automaton.grid_map_tools/scripts/utils.gd")

enum Mode {
	NONE,
	BRUSH_ADD,
	BRUSH_SMOOTH,
	BRUSH_REPLACE,
	BRUSH_REPLACE_SURFACE,
	BRUSH_REPLACE_TOP,
	BRUSH_PAINT_BUCKET,
}

var toolbar = null
var mode = Mode.NONE
var brush_shape = null
var brush_radius = 1
var mesh_idx = GridMap.INVALID_CELL_ITEM
var action_cooldown = DEFAULT_ACTION_COOLDOWN

var axis_locks = Vector3.ZERO
var axis_lock_pos = Vector3.ZERO

var current_gridmap = null
var utils = null
var mouse_down = false
var can_perform_action = true
var action_timer = null

var undo_redo = null

func _enter_tree():
	undo_redo = get_undo_redo()

	toolbar = Toolbar.instance()
	add_control_to_dock(DOCK_SLOT_LEFT_UL, toolbar)
	toolbar.connect("clear", self, "clear")
	toolbar.connect("brush_selected", self, "brush_selected")
	toolbar.connect("shape_selected", self, "shape_selected")
	toolbar.connect("mesh_selected", self, "mesh_selected")
	toolbar.connect("brush_radius_update", self, "brush_radius_update")
	toolbar.connect("brush_speed_update", self, "brush_speed_update")
	toolbar.connect("axis_locked", self, "axis_locked")
	toolbar.connect("smoothen", self, "smoothen")
	toolbar.connect("automata", self, "automata")
	toolbar.connect("make_hollow", self, "make_hollow")

	brush_shape = toolbar.Shape.CUBE

	utils = Utils.new()

	action_timer = Timer.new()
	action_timer.wait_time = action_cooldown
	action_timer.one_shot = false
	add_child(action_timer)
	action_timer.connect("timeout", self, "reset_action")
	action_timer.start()

func _exit_tree():
	remove_control_from_docks(toolbar)
	toolbar.free()
	action_timer.free()

func handles(object):
	return object is GridMap

func edit(object):
	print("Editing: ", object)
	current_gridmap = object
	toolbar.gridmap = object

#func make_visible(visible):
#	if visible:
#		toolbar.show()
#	else:
#		toolbar.hide()

func forward_spatial_gui_input(camera, event):
	var captured_event = false

	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.is_pressed():
			# @todo: commit action
			mouse_down = true
			captured_event = _call_brush(camera, event)

		if event.button_index == BUTTON_LEFT and !event.is_pressed():
			mouse_down = false
			captured_event = _call_brush(camera, event)

	if !can_perform_action:
		return mouse_down

	if event is InputEventMouseMotion:
		captured_event = _call_brush(camera, event)

	return captured_event

func _call_brush(camera, event) -> bool:
	if mode == Mode.NONE:
		return false

	return do_brush(camera, event)

func clear() -> void:
	current_gridmap.clear()

func brush_selected(brush: int) -> void:
	if brush == toolbar.Brush.ADD:
		mode = Mode.BRUSH_ADD
	elif brush == toolbar.Brush.SMOOTH:
		mode = Mode.BRUSH_SMOOTH
	elif brush == toolbar.Brush.REPLACE:
		mode = Mode.BRUSH_REPLACE
	elif brush == toolbar.Brush.REPLACE_SURFACE:
		mode = Mode.BRUSH_REPLACE_SURFACE
	elif brush == toolbar.Brush.REPLACE_TOP:
		mode = Mode.BRUSH_REPLACE_TOP
	elif brush == toolbar.Brush.PAINT_BUCKET:
		mode = Mode.BRUSH_PAINT_BUCKET
	else:
		mode = Mode.NONE

func mesh_selected(mesh: int) -> void:
	mesh_idx = mesh

func shape_selected(shape: int) -> void:
	brush_shape = shape

func brush_radius_update(new_radius: float) -> void:
	brush_radius = new_radius

func brush_speed_update(new_speed: float) -> void:
	action_cooldown = new_speed
	action_timer.wait_time = new_speed

func axis_locked(locks: Vector3, pos: Vector3) -> void:
	axis_locks = locks
	axis_lock_pos = pos

func smoothen() -> void:
	utils.gm_push_undo_stack(current_gridmap)
	undo_redo.create_action("Smoothen GridMap")
	undo_redo.add_undo_method(self, "undo_paint_gridmap")
	utils.gm_smoothen(current_gridmap)
	undo_redo.commit_action()
func automata() -> void:
	utils.gm_push_undo_stack(current_gridmap)
	undo_redo.create_action("Cellular Automata GridMap")
	undo_redo.add_undo_method(self, "undo_paint_gridmap")
	utils.gm_automata(current_gridmap)
	undo_redo.commit_action()

func make_hollow() -> void:
	utils.gm_push_undo_stack(current_gridmap)
	undo_redo.create_action("Make Hollow GridMap")
	undo_redo.add_undo_method(self, "undo_paint_gridmap")
	utils.gm_make_hollow(current_gridmap)
	undo_redo.commit_action()
func do_brush(camera, event) -> bool:
	var action_captured = false

	if current_gridmap == null:
		print("must set gridmap with Use Selected button")
		return action_captured

	if event is InputEventMouseMotion:
		if !mouse_down:
			return action_captured
	
		var hit_pos = utils.get_mouse_projection(current_gridmap, camera, event.position)

		var add_pos = null
		var intersection = null
		if hit_pos.has("position"):
			add_pos = hit_pos["position"]
			add_pos.y += 1
			intersection = hit_pos["position"]
		if hit_pos.has("moment"):
			add_pos = hit_pos["moment"]
		
		if hit_pos == null:
			action_captured = true
		else:
			_do_brush(add_pos, intersection)
			can_perform_action = false
			action_captured = true

	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.is_pressed():
			utils.gm_push_undo_stack(current_gridmap)
			undo_redo.create_action("Paint GridMap")
			undo_redo.add_undo_method(self, "undo_paint_gridmap")
			mouse_down = true
			action_captured = true
		if event.button_index == BUTTON_LEFT and !event.is_pressed():
			mouse_down = false
			action_captured = true
			undo_redo.commit_action()

	return action_captured

func _do_brush(position, intersection):
	if position == null:
		return

	if axis_locks.x != 0:
		position.x = axis_lock_pos.x
	if axis_locks.y != 0:
		position.y = axis_lock_pos.y
	if axis_locks.z != 0:
		position.z = axis_lock_pos.z

	var ctx = {
		"mesh": mesh_idx,
	}

	# Special accordance given to paint bucket so it will act on a single tile only
	if mode == Mode.BRUSH_PAINT_BUCKET:
		utils.gm_func_paint_bucket(current_gridmap, intersection, ctx)
		return

	var idxs = []
	if brush_shape == toolbar.Shape.CUBE:
		idxs = utils.gm_get_cube(current_gridmap, position, brush_radius)
	elif brush_shape == toolbar.Shape.SPHERE:
		idxs = utils.gm_get_sphere(current_gridmap, position, brush_radius)

	var fn = null
	if mode == Mode.BRUSH_ADD:
		fn = funcref(utils, "gm_func_add")
	elif mode == Mode.BRUSH_SMOOTH:
		fn = funcref(utils, "gm_func_smoothen")
	elif mode == Mode.BRUSH_REPLACE:
		fn = funcref(utils, "gm_func_replace")
	elif mode == Mode.BRUSH_REPLACE_SURFACE:
		fn = funcref(utils, "gm_func_replace_surface")
	elif mode == Mode.BRUSH_REPLACE_TOP:
		fn = funcref(utils, "gm_func_replace_top")

	utils.gm_do_func(current_gridmap, idxs, ctx, fn)
func reset_action():
	can_perform_action = true
func undo_paint_gridmap():
	var momento = utils.gm_pop_undo_stack(current_gridmap)
	utils.gm_restore_momento(current_gridmap, momento)
