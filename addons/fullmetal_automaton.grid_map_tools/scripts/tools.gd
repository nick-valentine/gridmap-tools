tool
extends Control

signal brush_selected(brush)
signal shape_selected(shape)
signal mesh_selected(mesh)
signal brush_radius_update(new_radius)
signal brush_speed_update(new_speed)
signal smoothen
signal automata
signal clear
signal axis_locked(locks, values)
signal make_hollow

enum Brush {
	NONE,
	ADD,
	SMOOTH,
	REPLACE,
	REPLACE_SURFACE,
	REPLACE_TOP,
	PAINT_BUCKET,
}

enum Shape {
	CUBE,
	SPHERE
}

var brush_names = {
	Brush.NONE: "None",
	Brush.ADD: "Add",
	Brush.SMOOTH: "Smoothen",
	Brush.REPLACE: "Replace",
	Brush.REPLACE_SURFACE: "Replace Surface",
	Brush.REPLACE_TOP: "Replace Top",
	Brush.PAINT_BUCKET: "Paint Bucket",
}

var shape_names = {
	Shape.CUBE: "Cube",
	Shape.SPHERE: "Sphere",
}

onready var brush_selector = $VBoxContainer/Brush/BrushSelector
onready var shape_selector = $VBoxContainer/Shape/ShapeSelector
onready var mesh_selector = $VBoxContainer/Mesh/MeshSelector

onready var lock_check_x = $VBoxContainer/LockChecks/LockX
onready var lock_check_y = $VBoxContainer/LockChecks/LockY
onready var lock_check_z = $VBoxContainer/LockChecks/LockZ

onready var lock_value_x = $VBoxContainer/LockValues/LineEditX
onready var lock_value_y = $VBoxContainer/LockValues/LineEditY
onready var lock_value_z = $VBoxContainer/LockValues/LineEditZ

var gridmap setget _set_gridmap, _get_gridmap

func _ready():
	brush_selector.clear()
	for i in brush_names:
		brush_selector.add_item(brush_names[i], i)
		
	shape_selector.clear()
	for i in shape_names:
		shape_selector.add_item(shape_names[i], i)

func _on_ClearButton_pressed():
	emit_signal("clear")

func _on_OptionButton_item_selected(index):
	if index < 0:
		return
	if index > brush_names.size():
		return
	emit_signal("brush_selected", index)

func _on_ShapeSelector_item_selected(index):
	if index < 0:
		return
	if index > shape_names.size():
		return
	emit_signal("shape_selected", index)

func _set_gridmap(new_gridmap: GridMap) -> void:
	mesh_selector.clear()
	mesh_selector.add_item("Remove", GridMap.INVALID_CELL_ITEM)
	gridmap = new_gridmap
	var lib = gridmap.mesh_library
	for i in lib.get_item_list():
		mesh_selector.add_item(lib.get_item_name(i), i)

func _get_gridmap() -> GridMap:
	return gridmap

func _on_MeshSelector_item_selected(id):
	emit_signal("mesh_selected", id - 1)

func _on_HSlider_value_changed(value):
	emit_signal("brush_speed_update", 1.1 - value)

func _on_SmoothButton_pressed():
	emit_signal("smoothen")

func _on_AutomataButton_pressed():
	emit_signal("automata")

func _on_Radius_value_changed(value):
	emit_signal("brush_radius_update", value)

func get_locked_axis() -> Vector3:
	return Vector3(
		lock_check_x.is_pressed(),
		lock_check_y.is_pressed(),
		lock_check_z.is_pressed()
	)

func get_locked_axis_values() -> Vector3:
	return Vector3(
		lock_value_x.text as int,
		lock_value_y.text as int,
		lock_value_z.text as int
	)

func _on_LockX_toggled(button_pressed):
	emit_signal("axis_locked", get_locked_axis(), get_locked_axis_values())

func _on_LockY_toggled(button_pressed):
	emit_signal("axis_locked", get_locked_axis(), get_locked_axis_values())

func _on_LockZ_toggled(button_pressed):
	emit_signal("axis_locked", get_locked_axis(), get_locked_axis_values())

func _on_LineEditX_text_changed(new_text):
	emit_signal("axis_locked", get_locked_axis(), get_locked_axis_values())

func _on_LineEditY_text_changed(new_text):
	emit_signal("axis_locked", get_locked_axis(), get_locked_axis_values())

func _on_LineEditZ_text_changed(new_text):
	emit_signal("axis_locked", get_locked_axis(), get_locked_axis_values())

func _on_MakeHollowButton_pressed():
	emit_signal("make_hollow")
