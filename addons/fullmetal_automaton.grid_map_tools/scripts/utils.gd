extends Node

const UNDO_STACK_MAX_SIZE = 20

const AUTOMATA_MIN_ALIVE = 3
const AUTOMATA_MAX_ALIVE = 7

enum ConvolutionAction {
	REMOVE,
	ALIVE,
}

var convolution_kernel = [
	[
		[1, 1, 1],
		[1, 2, 1],
		[1, 1, 1],
	],
	[
		[1, 2, 1],
		[2, 4, 2],
		[1, 2, 1],
	],
	[
		[1, 1, 1],
		[1, 2, 1],
		[1, 1, 1]
	]
]

var convolution_cutoff = 0.4


func convolution_normalize(kernel: Array) -> Array:
	var sum : float = 0
	for i in range(0, kernel.size()):
		for j in range(0, kernel[i].size()):
			for k in range(0, kernel[i][j].size()):
				sum += kernel[i][j][k]
	
	var out = []
	for i in range(0, kernel.size()):
		out.append([])
		for j in range(0, kernel[i].size()):
			out[i].append([])
			for k in range(0, kernel[i][j].size()):
				out[i][j].append(kernel[i][j][k] / sum)
	return out

func get_mouse_projection(gm: GridMap, camera, mouse) -> Dictionary:
	var origin = camera.project_ray_origin(mouse)
	var dir = camera.project_ray_normal(mouse)
	
	return gm_raycast(gm, origin, dir)	

func gm_intersects(gm: GridMap, position: Vector3) -> bool:
	var map_pos : Vector3 = gm.world_to_map(position)
	var cell : int = gm.get_cell_item(map_pos.x, map_pos.y, map_pos.z)
	if cell != gm.INVALID_CELL_ITEM:
		return true
	return false

func gm_raycast(gm: GridMap, origin: Vector3, dir: Vector3) -> Dictionary:
	if gm_intersects(gm, origin):
		return {
			"position": gm.world_to_map(origin)
		}

	var moment = origin

	var pos = origin
	var unit = 0.2
	var d = 0.0
	var max_distance = 800.0
	while d < max_distance:
		pos += dir * unit
		if gm_intersects(gm, pos):
			return {
				"position": gm.world_to_map(pos),
				"moment": gm.world_to_map(moment),
			}
		d += unit
		moment = pos
	return {}

func gm_get_sphere(gm: GridMap, origin: Vector3, radius: int) -> Array:
	var out = []
	for i in range(-1 * radius, 1 + radius):
		for j in range(-1 * radius, 1 + radius):
			for k in range(-1 * radius, 1 + radius):
				var pos = origin + Vector3(i, j, k)
				if (origin - pos).length() < radius:
					out.append(pos)
	return out

func gm_get_cube(gm: GridMap, origin: Vector3, radius: int) -> Array:
	var out = []
	for i in range(-1 * radius, 1 + radius):
		for j in range(-1 * radius, 1 + radius):
			for k in range(-1 * radius, 1 + radius):
				out.append(origin + Vector3(i, j, k))
	return out

func gm_do_func(gm: GridMap, idxs: Array, ctx: Dictionary, fn) -> void:
	for i in idxs:
		fn.call_func(gm, i, ctx)

func gm_func_add(gm: GridMap, idx: Vector3, ctx: Dictionary) -> void:
	var mesh = 0
	if ctx.has("mesh"):
		mesh = ctx["mesh"]
	gm.set_cell_item(idx.x, idx.y, idx.z, mesh, 0)

func gm_func_smoothen(gm: GridMap, idx: Vector3, ctx: Dictionary) -> void:
	var action_list = []
	action_list.append(gm_convolution(gm, idx, convolution_normalize(convolution_kernel), convolution_cutoff))
	gm_do_action(gm, action_list)

func gm_func_replace(gm: GridMap, idx: Vector3, ctx: Dictionary) -> void:
	var mesh = 0
	if ctx.has("mesh"):
		mesh = ctx["mesh"]
	if gm.get_cell_item(idx.x, idx.y, idx.z) != GridMap.INVALID_CELL_ITEM:
		gm.set_cell_item(idx.x, idx.y, idx.z, mesh, 0)

func gm_func_replace_surface(gm: GridMap, idx: Vector3, ctx: Dictionary) -> void:
	var mesh = 0
	if ctx.has("mesh"):
		mesh = ctx["mesh"]
	if gm.get_cell_item(idx.x, idx.y, idx.z) != GridMap.INVALID_CELL_ITEM:
		if gm_touches_air(gm, idx):
			gm.set_cell_item(idx.x, idx.y, idx.z, mesh, 0)

func gm_func_replace_top(gm: GridMap, idx: Vector3, ctx: Dictionary) -> void:
	var mesh = 0
	if ctx.has("mesh"):
		mesh = ctx["mesh"]
	if gm.get_cell_item(idx.x, idx.y, idx.z) == GridMap.INVALID_CELL_ITEM:
		return
	if gm.get_cell_item(idx.x, idx.y + 1, idx.z) == GridMap.INVALID_CELL_ITEM:
		gm.set_cell_item(idx.x, idx.y, idx.z, mesh, 0)

func gm_func_paint_bucket(gm: GridMap, idx: Vector3, ctx: Dictionary) -> void:
	var mesh = 0
	if ctx.has("mesh"):
		mesh = ctx["mesh"]
	var paint = {idx: true}
	var old_size = 0

	var from_mesh : int = gm.get_cell_item(idx.x, idx.y, idx.z)
	if from_mesh == GridMap.INVALID_CELL_ITEM:
		return

	while paint.size() != old_size:
		old_size = paint.size()
		for i in paint:
			if paint[i] == true:
				paint[i] = false
				paint = gm_neighbors_of_type(paint, gm, i, from_mesh)

	print("painting %d tiles" % paint.size())
	for i in paint:
		gm.set_cell_item(i.x, i.y, i.z, mesh, 0)


func gm_do_action(gm: GridMap, action_list: Array) -> void:
	for i in action_list:
		var pos = i["position"]
		if i["action"] == ConvolutionAction.REMOVE:
			gm.set_cell_item(pos.x, pos.y, pos.z, GridMap.INVALID_CELL_ITEM)
		if i["action"] == ConvolutionAction.ALIVE:
			gm.set_cell_item(pos.x, pos.y, pos.z, i["mesh"])

func gm_get_max_mesh(counts: Dictionary) -> int:
	var max_mesh = GridMap.INVALID_CELL_ITEM
	var max_mesh_count = 0
	for i in counts:
		if counts[i] > max_mesh_count:
			max_mesh = i
			max_mesh_count = counts[i]
	return max_mesh

func gm_smoothen(gm: GridMap):
	var action_list = []
	for i in gm.get_used_cells():
		action_list.append(gm_convolution(gm, i, convolution_normalize(convolution_kernel), convolution_cutoff))
	gm_do_action(gm, action_list)

func gm_convolution(gm: GridMap, idx: Vector3, kernel: Array, cutoff: float) -> Dictionary:
	var half_width = kernel.size() / 2
	var half_height = kernel[0].size() / 2
	var half_depth = kernel[0][0].size() / 2

	var mesh_counts = {}

	var sum : float = 0.0
	for i in range(-1 * half_width, 1 + half_width):
		for j in range(-1 * half_height, 1 + half_height):
			for k in range(-1 * half_depth, 1 + half_depth):
				var cell : int = gm.get_cell_item(idx.x + i, idx.y + j, idx.z + k)
				if cell != GridMap.INVALID_CELL_ITEM:
					sum += kernel[i][j][k]
					if !mesh_counts.has(cell):
						mesh_counts[cell] = 0
					mesh_counts[cell] += 1
	var max_mesh = gm_get_max_mesh(mesh_counts)
	if sum < cutoff:
		return {
			"action": ConvolutionAction.REMOVE,
			"position": idx,
			"mesh": max_mesh,
		}
	else:
		return {
			"action": ConvolutionAction.ALIVE,
			"position": idx,
			"mesh": max_mesh,
		}

func gm_automata(gm: GridMap):
	var action_list = []
	for i in gm.get_used_cells():
		action_list.append(gm_automata_cell(gm, i))
	gm_do_action(gm, action_list)

func gm_automata_cell(gm: GridMap, idx: Vector3) -> Dictionary:
	var mesh_counts = {}
	var alive = 0
	for i in gm_neighbors(gm, idx):
		var cell : int = gm.get_cell_item(i.x, i.y, i.z)
		if cell != GridMap.INVALID_CELL_ITEM:
			alive += 1
			if !mesh_counts.has(cell):
				mesh_counts[cell] = 0
			mesh_counts[cell] += 1
	var max_mesh = gm_get_max_mesh(mesh_counts)
	if alive < AUTOMATA_MIN_ALIVE || alive > AUTOMATA_MAX_ALIVE:
		return {
			"action": ConvolutionAction.REMOVE,
			"position": idx,
			"mesh": max_mesh,
		}
	else:
		return {
			"action": ConvolutionAction.ALIVE,
			"position": idx,
			"mesh": max_mesh,
		}

func gm_make_hollow(gm: GridMap):
	var action_list = []
	for i in gm.get_used_cells():
		action_list.append(_gm_make_hollow(gm, i))
	gm_do_action(gm, action_list)

func _gm_make_hollow(gm: GridMap, idx: Vector3) -> Dictionary:
	var cell : int = gm.get_cell_item(idx.x, idx.y, idx.z)
	if !gm_touches_air(gm, idx):
		return {
			"action": ConvolutionAction.REMOVE,
			"position": idx,
			"mesh": cell,
		}
	else:
		return {
			"action": ConvolutionAction.ALIVE,
			"position": idx,
			"mesh": cell,
		}

func gm_neighbors(gm: GridMap, idx: Vector3) -> Array:
	var out = []
	for i in range(idx.x - 1, idx.x + 2):
		for j in range(idx.y - 1, idx.y + 2):
			for k in range(idx.z - 1, idx.z + 2):
				out.append(Vector3(i, j, k))
	return out

func gm_touches_air(gm: GridMap, idx: Vector3) -> bool:
	var neighbors = gm_neighbors(gm, idx)
	for i in neighbors:
		if gm.get_cell_item(i.x, i.y, i.z) == GridMap.INVALID_CELL_ITEM:
			return true
	return false

func gm_neighbors_of_type(out: Dictionary, gm: GridMap, idx: Vector3, mesh: int) -> Dictionary:
	for i in gm_neighbors(gm, idx):
		var cell : int = gm.get_cell_item(i.x, i.y, i.z)
		if cell == mesh:
			if !out.has(i):
				out[i] = true
	return out


func gm_create_momento(gm: GridMap) -> Array:
	var momento = []
	for i in gm.get_used_cells():
		momento.append({
			"position": i,
			"mesh": gm.get_cell_item(i.x, i.y, i.z),
			"orientation": gm.get_cell_item_orientation(i.x, i.y, i.z),
		})
	return momento

func gm_restore_momento(gm: GridMap, momento: Array) -> void:
	gm.clear()
	for i in momento:
		var pos = i["position"]
		var mesh = i["mesh"]
		var orientation = i["orientation"]
		gm.set_cell_item(pos.x, pos.y, pos.z, mesh, orientation)

func gm_push_undo_stack(gm: GridMap) -> void:
	var momento = gm_create_momento(gm)
	if !gm.has_meta('undo_stack'):
		gm.set_meta('undo_stack', [])
	var undo_stack = gm.get_meta('undo_stack')
	undo_stack.push_back(momento)
	if undo_stack.size() > UNDO_STACK_MAX_SIZE:
		undo_stack.pop_front()

func gm_pop_undo_stack(gm: GridMap) -> Dictionary:
	if !gm.has_meta('undo_stack'):
		gm.set_meta('undo_stack', [])
	var undo_stack = gm.get_meta('undo_stack')
	var momento = undo_stack.back()
	undo_stack.pop_back()
	return momento
